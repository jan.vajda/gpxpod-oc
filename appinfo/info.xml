<?xml version="1.0"?>
<info>
    <id>gpxpod</id>
    <name>GpxPod</name>
    <summary lang="en">Display, analyse, compare and share gpx tracks</summary>
    <description>
# GpxPod Nextcloud application

Application to display gpx, kml, igc, fit and tcx files collections,
view elevation profiles and tracks stats, filter tracks,
 color tracks by speed, elevation or pace and compare divergent parts of similar tracks.

If you want to help translating this app in your language, go to [GpxPod Crowdin project](https://crowdin.com/project/gpxpod).

GpxPod :

* can display gpx/kml/tcx/igc/fit files anywhere in your files, files shared with you, files in folders shared with you. fit files will be converted and displayed only if **GpsBabel** is found on the server system.
* supports metric, english and nautical measure systems
* draws elevation, speed or pace interactive chart
* displays geotagged pictures found in selected directory
* generates public links pointing to a track/folder. This link can be used if the file/folder is shared by public link
* allows you to move selected track files
* can correct tracks elevations if SRTM.py (gpxelevations) is found on the server's system
* can make global comparison of multiple tracks
* can make visual pair comparison of divergent parts of similar tracks
* allows users to add personal map tile servers
* saves/restores user options values
* allows user to manually set track line colors
* detects browser timezone
* loads extra marker symbols from GpxEdit if installed
* works with encrypted data folder (server side encryption)
* proudly uses Leaflet with lots of plugins to display the map
* is compatible with SQLite, MySQL and PostgreSQL databases
* adds possibility to view .gpx files directly from the "Files" app

This app is tested on Nextcloud 14 with Firefox and Chromium.

This app is under development.

Link to Nextcloud application website : https://apps.nextcloud.com/apps/gpxpod

## Donation

I develop this app during my free time.

* Donate on Paypal : [There is a donation link here](https://gitlab.com/eneiluj/gpxpod-oc#donation) (you don't need a paypal account).
* Bitcoin : 1FfDVdPK8mZHB84EdN67iVgKCmRa3SwF6r
* Monero : 43moCXnskkeJNf1MezHnjzARNpk2BRvhuRA9vzyuVAkTYH2AE4L4EwJjC3HbDxv9uRBdsYdBPF1jePLeV8TpdnU7F9FN2Ao

## Install

See the [AdminDoc](https://gitlab.com/eneiluj/gpxpod-oc/wikis/admindoc) for installation details (allow SRTM elevation correction, integration in "Files" app, tcx, igc, fit compatibility...)

## Known issues

* bad management of file names including simple or double quotes
* _WARNING_, kml conversion will NOT work with recent kml files using the proprietary "gx:track" extension tag.

Any feedback will be appreciated.

    </description>
    <version>2.3.1</version>
    <licence>agpl</licence>
    <author>Julien Veyssier (@eneiluj)</author>
    <namespace>GpxPod</namespace>
    <documentation>
        <user>https://gitlab.com/eneiluj/gpxpod-oc/wikis/userdoc</user>
        <admin>https://gitlab.com/eneiluj/gpxpod-oc/wikis/admindoc</admin>
        <developer>https://gitlab.com/eneiluj/gpxpod-oc/wikis/devdoc</developer>
    </documentation>
    <category>tools</category>
    <category>social</category>
    <category>multimedia</category>
    <website>https://gitlab.com/eneiluj/gpxpod-oc</website>
    <bugs>https://gitlab.com/eneiluj/gpxpod-oc/issues</bugs>
    <screenshot>https://gitlab.com/eneiluj/gpxpod-oc/uploads/717322462e1143e477a9cf41d7498744/screen1.jpg</screenshot>
    <screenshot>https://gitlab.com/eneiluj/gpxpod-oc/uploads/edb4334127d054f017e656d964efcc66/screen2.jpg</screenshot>
    <screenshot>https://gitlab.com/eneiluj/gpxpod-oc/uploads/0ffda108daeede88dcd432a1021d21e4/screen3.jpg</screenshot>
    <dependencies>
        <database min-version="9.4">pgsql</database>
        <database>sqlite</database>
        <database min-version="5.5">mysql</database>
        <nextcloud min-version="14.0" max-version="14.9"/>
    </dependencies>
</info>
